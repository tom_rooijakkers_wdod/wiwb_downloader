# WIWB DOWNLOADER

WIWB Downloader is a python package to make downloading from the WIWB API easy to use.
The code is open source and suggestions are welcome.

## Features

- Download Grid data
- Download ModelGrid data
- Download TimeSeries dat
- Use shapefile to get data only for your area of interest.
- Export data to csv.
- Command line usage

## Installation

Because this package uses geopandas and other c based python libraries, 
it is recommended to first create a new conda environment with the environment.yml file. \
The environment.yml is included in the git repository, this will ensure the package is working properly.
To make this easier, you can run the following command to make a new environment called _wiwb_downloader_:
```sh
conda env create -f https://gitlab.com/tcadee/wiwb_downloader/-/raw/master/environment.yml
```

You can then install via pip:
```sh
pip install git+https://gitlab.com/tcadee/wiwb_downloader#egg=wiwb_downloader
```

Furthermore, if you want to use the WIWB-API your IP adress needs to be whitelisted. You can contact Het Waterschapshuis (HWH) for access.

# Usage

### Username and password
The first thing to be able to download data from the WIWB API is to set your username and password.
These need to be set in your environment variables:
username: _WIWB_USERNAME_ 
password: _WIWB_PASSWORD_

### Parameter file
To download data you need a parameters json file to specify your download request.
This file contains the following structure:
```json
{
    "StartDate": In any well known datetime format, eg: YYYY:MM:DD,
    "EndDate": In any well known datetime format, eg: YYYY:MM:DD,
    "Extent": Either list with coordinates in EPSG 28992 or path to shapefile,
    "DataPath" : Optional, if you want to save your data in a specific location,
    "DataSources": {
        DATA_SOURCE_NAME : {
            "Variables": List of variables to download for DATA_SOURCE_NAME,
            "TimeAhead": Only used when requesting modelgrids, explanation can be found below.
            "Offset": Only used when requesting modelgrids, explanation can be found below.
        }
        DATA_SOURCE_NAME : {
            you can add as many data sources as you would like.
        }
    }
}
```
Extent can either be a path to a shapefile or a list with coordinates. The format for this coordinate list has to be [xll, yll, xur, lur] (lowerleft and upright). This will function as a bouding box for your request.

DATA_SOURCE_NAME has to be the same name as provided in the WIWB technical instructions. 
The same holds for the variable names.

TimeAhead and Offset are used for model grids. For example: The Harmonie model returns 48 hour predictions and is generated every 6 hours. 
If you only want to retrieve data from 6-12 hours for every model you need to set Offset to '6h' and TimeAhead to '12h'. 
For minutes use '#m' and for days use '#d'.

For parameter.json examples: 
https://gitlab.com/tcadee/wiwb_downloader/-/tree/master/examples

For Data Source names and variables:
http://meteobase.nl:8080/meteobase/downloads/fixed/literatuur/P951_Rapportage-Technische_instructies_WIWB_API_V5.0.pdf

### Download

To download, run the following command in your command prompt:
```sh
wiwb-download [parameter_file]
```

The script is equipped with progress bars to monitor your progress.

## License

MIT
