import os
from datetime import datetime, timedelta

import geopandas as gpd
import requests
from numpy import linspace
from requests.auth import HTTPBasicAuth
from shapely.geometry import Polygon

from .config import CONFIG


class Downloader():
    """Base class for downloading WIWB data.

    This class contains the most important methods for downloading data.

    Attributes
    ----------
    data_source : string
        The data source name.
    start_date : datetime
        Start date for download request.
    end_date : datetime
        End date for download request.
    header : dict
        Header for download request.
    auth : dict
        Authentication for download request.
    url : string
        API url.
    data_source_id : string
        Data source id.
    counter : int
        Counter for progress bar.
    data_path : Path to download data to.
    """

    def __init__(self, data_source, type, start_date, end_date):
        self.data_source = data_source
        self.end_date = end_date
        self.start_date = start_date
        self.header = self.__get_header()
        self.auth = self.__get_authentication()
        self.url = f"https://wiwb.hydronet.com/api/{type}/get"
        self.data_source_id = CONFIG[data_source]['DataSourceID']
        self.counter = 0
        self.data_path = None

    def __get_header(self):
        """Get header for download request

        Returns
        -------
        dict
            Header for download request
        """
        header = {
            "Accept": "image/tiff",
            "Content-Type": "application/json"
        }
        return header

    def __get_authentication(self):
        """Get authentication for download request.

        Returns
        -------
        HTTPBasicAuth
            Authentication token
        """
        username = os.environ['WIWB_USERNAME']
        password = os.environ['WIWB_PASSWORD']
        auth = HTTPBasicAuth(username, password)
        return auth

    def parse_timedelta(self, step):
        """Parse timedelta string.

        Parameters
        ----------
        step : string
            string containing timedelta.

        Returns
        -------
        timedelta
            parsed timedelta.
        """
        quantity = int(step[:-1])
        value = step[-1]
        if value == 'm':
            return timedelta(minutes=quantity)
        elif value == 'h':
            return timedelta(hours=quantity)
        elif value == 'd':
            return timedelta(days=quantity)

    def datetimeToString(self, date):
        """Convert datetime to string.

        Parameters
        ----------
        date : datetime
            datetime to convert.

        Returns
        -------
        string
            converted datetime in string format.
        """
        return date.strftime("%Y%m%d%H%M%S")

    def stringToDateTime(self, date):
        """Convert string to datetime.

        Parameters
        ----------
        date : string
            string to convert.

        Returns
        -------
        datetime
            converted string in datetime format.
        """
        return datetime.strptime(date, "%Y%m%d%H%M%S")

    def get_extent(self, extent):
        """Get extent from extent argument.

        If extent is shapefile path, get the total_bounds from this shapefile.

        Parameters
        ----------
        extent : list/string
            extent for download request

        Returns
        -------
        (MultiPolygon/False, list)
            Shape if extent is shapefile path, or False and extent.
        """
        if isinstance(extent, list):
            shape = False
            extent = extent
        else:
            self.shape_df = gpd.read_file(extent).set_crs(epsg=28992)
            shape = self.shape_df.unary_union
            extent = self.shape_df.total_bounds
        return shape, extent

    def get_cells_to_keep(self, griddef):
        """Check which gridcells fall within the shapefile.

        Return all cells if no shapefile is provided.

        Parameters
        ----------
        griddef : dict
            Grid definition from download request.

        Returns
        -------
        list
            list with cells to keep.
        """
        rows, cols = griddef['Rows'], griddef['Columns']
        if self.shape and self.epsg != 10000:
            xmin = griddef['Xll']
            ymax = griddef['Yur']
            xmax = griddef['Xur']
            ymin = griddef['Yll']
            xstep = (xmax-xmin) / cols
            ystep = (ymax-ymin) / rows
            grid_df = gpd.GeoSeries([
                Polygon([(x, y), (x+xstep, y), (x+xstep, y-ystep), (x, y-ystep)])
                for y in linspace(ymax, ymin, rows, True)
                for x in linspace(xmin, xmax, cols, True)
            ])
            grid_list = grid_df.set_crs(epsg=self.epsg).to_crs(epsg=28992).to_list()
            cells = [i for i, cell in enumerate(grid_list) if cell.intersects(self.shape)]
        else:
            cells = list(range(int(rows * cols)))
        return cells

    def make_request(self, body):
        """Make API request and return content.

        Parameters
        ----------
        body : dict
            Request body.

        Returns
        -------
        dict
            json content from request.
        """
        response = requests.post(
            url=self.url,
            auth=self.auth,
            headers=self.header,
            json=body
        )
        response.encoding = 'utf-8-sig'
        return response.json()

    def save_output(self, output_df):
        """Save output to csv file.

        Parameters
        ----------
        output_df : DataFrame
            DataFrame containing downloaded data.
        """
        file_out = f'{self.data_source.lower().replace(" ","_")}.csv'
        output_df.to_csv(os.path.join(self.data_path, file_out))
