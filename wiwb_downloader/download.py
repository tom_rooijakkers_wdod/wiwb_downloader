import json
from argparse import ArgumentParser

from dateutil.parser import parse

from .config import CONFIG
from .grid_downloader import GridDownloader
from .modelgrid_downloader import ModelGridDownloader
from .timeseries_downloader import TimeSeriesDownloader


def download(data_source, extent, start_date, end_date, data_path, args):
    """Download data source from WIWB

    Parameters
    ----------
    data_source : string
        Data source name.
    extent : list/string
        Extent coordinates or path to shapefile.
    start_date : datetime
        Start date for download request.
    end_date : datetime
        End date for download request.
    data_path : string
        Path to save data to.
    args : dict
        Additional arguments for download request.
    """
    start_date, check_start_date = __check_start_date(data_source, start_date, end_date)
    if check_start_date:
        type = CONFIG[data_source]['Type']
        if type == "grids":
            downloader = GridDownloader(data_source, extent, type, start_date, end_date, args)
        elif type == "modelgrids":
            downloader = ModelGridDownloader(data_source, extent, type, start_date, end_date, args)
        elif type == "timeseries":
            downloader = TimeSeriesDownloader(data_source, extent, type, start_date, end_date, args)
        downloader.data_path = data_path
        downloader.download()


def __check_start_date(data_source, start_date, end_date):
    data_source_start = CONFIG[data_source]['StartDate']
    if start_date < data_source_start:
        print(f'Data for {data_source} only available from {data_source_start}. Starting download from this date.')
        start_date = data_source_start
        if start_date > end_date:
            print(f'Startdate > Enddate, terminating download for {data_source}')
            return start_date, False
    return start_date, True


def main():
    parser = ArgumentParser(description='Download data from WIWB')
    parser.add_argument('params', help='parameter json with download instructions')
    args = parser.parse_args()
    with open(args.params) as json_file:
        arg_dict = json.load(json_file)
    start_date = parse(arg_dict['StartDate'])
    end_date = parse(arg_dict['EndDate'])
    extent = arg_dict.get('Extent', False)
    data_path = arg_dict.get('DataPath', '')
    for data_source, args in arg_dict['DataSources'].items():
        download(data_source, extent, start_date, end_date, data_path, args)


if __name__ == "__main__":
    main()
