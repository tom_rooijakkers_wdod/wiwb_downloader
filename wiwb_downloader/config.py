from datetime import datetime

OFFSET_CONFIG = {
    "Harmonie": {
        "Offset": "0h",
        "TimeAhead": "48h"
    },
    "SATDATA3.0 Evapotranspiration": {
        "Offset": "0d",
        "TimeAhead": "3d"
    },
    "SATDATA3.0 Evapotranspiration Reanalysis": {
        "Offset": "0d",
        "TimeAhead": "3d"
    }
}
CONFIG = {
    "KNMI IRC Realtime": {
        "Type": "grids",
        "EPSG": 10000,
        "Step": "5m",
        "DataSourceID": "Knmi.International.Radar.Composite",
        "StartDate": datetime(2018, 12, 7)
    },
    "KNMI IRC Early Reanalysis": {
        "Type": "grids",
        "Step": "5m",
        "DataSourceID": "Knmi.International.Radar.Composite.Early.Reanalysis",
        "StartDate": datetime(2019, 7, 19)
    },
    "KNMI IRC Final Reanalysis": {
        "Type": "grids",
        "Step": "5m",
        "DataSourceID": "Knmi.International.Radar.Composite.Final.Reanalysis",
        "StartDate": datetime(2019, 6, 19)
    },
    "Ongecorrigeerde Radar": {
        "Type": "grids",
        "Step": "5m",
        "DataSourceID": "Knmi.Radar.Uncorrected",
        "StartDate": datetime(2017, 9, 1)
    },
    "Eps meteo parameters": {
        "Type": "Ensemble timeseries",
        "DataSourceID": "Knmi.RegionalEps",
        "Step": "6h",
        "StartDate": datetime(2002, 8, 26)
    },
    "Harmonie": {
        "Type": "modelgrids",
        "DataSourceID": "Knmi.Harmonie",
        "Step": "6h",
        "EPSG": 4326,
        "StartDate": datetime(2017, 10, 25)
    },
    "Iris stations Ongevalideerd": {
        "Type": "timeseries",
        "Step": "1d",
        "DataSourceID": "Knmi.IrisUnvalidated",
        "StartDate": datetime(2010, 4, 25)
    },
    "Iris stations gevalideerd": {
        "Type": "timeseries",
        "Step": "1d",
        "DataSourceID": "Knmi.IrisValidated",
        "StartDate": datetime(1905, 12, 31)
    },
    "Referentie gasverdamping": {
        "Type": "timeseries",
        "Step": "1d",
        "DataSourceID": "Knmi.Evaporation",
        "StartDate": datetime(1951, 1, 1)
    },
    "Eps buitenwaterstanden": {
        "Type": "Ensemble timeseries",
        "DataSourceID": "Knmi.WaterSetupEps",
        "Step": 6,
        "StartDate": datetime(2017, 11, 28)
    },
    "Waarschuwingen scheepvaart kust": {
        "Type": "Event",
        "DataSourceID": "KNMI.Naval.Warnings",
        "StartDate": datetime(2017, 12, 5)
    },
    "Waarschuwingen scheepvaart NL": {
        "Type": "Event",
        "DataSourceID": "KNMI.Naval.Forecast",
        "StartDate": datetime(2017, 12, 5)
    },
    "Weerwaarschuwingen": {
        "Type": "timeseries",
        "DataSourceID": "KNMI.Warnings",
        "StartDate": datetime(2017, 12, 5)
    },
    "Waqua": {
        "Type": "Modeltimeseries",
        "DataSourceID": "KNMI.WaquaTs",
        "Step": "12h",
        "StartDate": datetime(2017, 12, 3)
    },
    "KNMI AWS Stations": {
        "Type": "timeseries",
        "Step": "10m",
        "DataSourceID": "KNMI.AwsTenMinutes",
        "StartDate": datetime(2017, 11, 25)
    },
    "SATDATA3.0 Evapotranspiration": {
        "Type": "modelgrids",
        "DataSourceID": "Satdata.Evapotranspiration",
        "Step": "1d",
        "EPSG": 28992,
        "StartDate": datetime(2020, 1, 1)
    },
    "SATDATA3.0 Evapotranspiration Reanalysis": {
        "Type": "grids",
        "DataSourceID": "Satdata.Evapotranspiration.Reanalysis",
        "Step": "1d",
        "EPSG": 28992,
        "StartDate": datetime(2020, 1, 1)
    }
}
