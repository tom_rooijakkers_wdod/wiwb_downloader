import numpy as np
import pandas as pd
from tqdm import tqdm

from .config import CONFIG
from .downloader import Downloader


class GridDownloader(Downloader):
    """Class to download grid data from WIWB.

    Extends the Download class.

    Attributes
    ----------
    variables : list
        list of variables to download.
    step : timedelta
        Data source frequency.
    status_bar : tqdm
        Progress bar.
    shape : bool/MultiPolygon
        Shape from shapefile or False if none provided.
    extent : list
        Extent coordinates.
    epsg : string
        Epsg code for downloaded grids.
    """

    def __init__(self, data_source, extent, type, start_date, end_date, args):
        super().__init__(data_source, type, start_date, end_date)
        self.variables = args['Variables']
        self.step = self.parse_timedelta(CONFIG[data_source]['Step'])
        steps = (self.end_date-self.start_date) / self.step
        self.status_bar = tqdm(total=steps, desc=f'{data_source}: Downloading data')
        self.epsg = CONFIG[data_source]['EPSG']
        self.shape, self.extent = self.get_extent(extent)

    def __get_body(self):
        """Get body for download request.

        Returns
        -------
        dict
            Request body.
        """
        start_date = self.datetimeToString(self.start_date)
        end_date = self.datetimeToString(self.end_date)
        body = {
            "Readers": [{
                "DataSourceCode": self.data_source_id,
                "Settings": {
                    "StartDate": start_date,
                    "EndDate": end_date,
                    "VariableCodes": self.variables,
                    "Extent": {
                        "Xll": self.extent[0],
                        "Yll": self.extent[1],
                        "Xur": self.extent[2],
                        "Yur": self.extent[3],
                        "SpatialReference": {
                            "Epsg": 28992
                        }
                    }
                }
            }],
            "Exporter": {
                "DataFormatCode": "json",
                "Settings": {
                    "Formatting": "Indented"
                }
            }
        }
        return body

    def __parse_json(self, content):
        """Parse json content into dictionary

        Parameters
        ----------
        content : dict
            Json content from download.

        Returns
        -------
        dict
            Dictionary containing downloaded data.
        """
        output_dict = {}
        self.status_bar.set_description_str(f"{self.data_source}: Parsing data")
        grid_def = content['Data'][0]['GridDefinition']
        self.cells = self.get_cells_to_keep(grid_def)
        for grid in content['Data']:
            if self.shape:
                data = np.array(grid['Data'])[self.cells].tolist()
            else:
                data = np.array(grid['Data']).tolist()
            output_dict[self.counter] = [self.stringToDateTime(grid['StartDate']),
                                         grid['VariableCode']] + data
            self.counter += 1
            self.status_bar.update(self.counter)
        return output_dict

    def download(self):
        """Download data for this data source.
        """
        body = self.__get_body()
        content = self.make_request(body)
        output_dict = self.__parse_json(content)
        columns = ['StartDate', 'Variable'] + list(range(len(self.cells)))
        output_df = pd.DataFrame.from_dict(output_dict, orient='index', columns=columns)
        self.save_output(output_df)
