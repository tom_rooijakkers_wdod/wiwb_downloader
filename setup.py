from setuptools import setup, find_packages

setup(
    name='wiwb-downloader',
    version='0.2.1',
    description='Package to download data from WIWB',
    classifiers=[
        "Programming Language :: Python :: 3.8"],
    keywords='api wiwb waterschap knmi',
    url='https://gitlab.com/hhrhhsk/hhsk/wiwb-downloader',
    author='Tobias Cadée',
    author_email='tcadee@hhsk.nl',
    license='MIT',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    entry_points={
        'console_scripts': [
            'wiwb_download = wiwb_downloader.download:main'
         ]
    })
