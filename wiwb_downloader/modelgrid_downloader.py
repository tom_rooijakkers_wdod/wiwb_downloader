from datetime import timedelta

import numpy as np
import pandas as pd
from tqdm import tqdm

from .config import CONFIG
from .config import OFFSET_CONFIG as OC
from .downloader import Downloader


class ModelGridDownloader(Downloader):
    """Class to download model grid data from WIWB.

    Extends the Download class.

    Attributes
    ----------
    variables : list
        list of variables to download.
    step : timedelta
        Data source frequency.
    shape : bool/MultiPolygon
        Shape from shapefile or False if none provided.
    extent : list
        Extent coordinates.
    epsg : string
        Epsg code for downloaded grids.
    time_ahead : timedelta
        Period to retrieve data, from model date.
    offset: timedelta
        Time from model date to retrieve data.
    date_range : tqdm
        Model dates to get data from, and also progress bar.
    """

    def __init__(self, data_source, extent, type, start_date, end_date, args):
        super().__init__(data_source, type, start_date, end_date)
        self.variables = args['Variables']
        self.step = self.parse_timedelta(CONFIG[data_source]['Step'])
        self.offset = self.parse_timedelta(args.get("Offset", OC[data_source]['Offset']))
        self.time_ahead = self.parse_timedelta(args.get("TimeAhead", OC[data_source]['TimeAhead']))
        self.epsg = CONFIG[data_source]['EPSG']
        status = f"{self.data_source}: Setting extent"
        self.date_range = tqdm(self.__get_date_range(), desc=status)
        self.shape, self.extent = self.get_extent(extent)
        self.cells = self.__get_cells()

    def __get_cells(self):
        """Get cells to keep from extent.

        Returns
        -------
        list
            Gridcells to keep after download request
        """
        body = self.__get_body(self.start_date, grid_def=True)
        grid_def = self.make_request(body)['Data'][0]['Grids'][0]['GridDefinition']
        cells = self.get_cells_to_keep(grid_def)
        return cells

    def __get_date_range(self):
        """Get daterange from start and end date.

        Returns
        -------
        list
            List containing all model dates between start and end date.
        """
        date_range = []
        start = self.start_date
        if start.hour != 0:
            start -= timedelta(hours=start.hour)
        while start <= self.end_date:
            date_range.append(start)
            start += self.step
        return date_range

    def __get_body(self, model_date, grid_def=False):
        """Get body for download request.

        Parameters
        ----------
        grid_def : bool/dict
            Grid definition to download sample or False if full download.

        Returns
        -------
        dict
            Request body.
        """
        if grid_def:
            offset = timedelta(hours=0)
            time_ahead = timedelta(hours=24)
            variables = [self.variables[0]]
        else:
            offset = self.offset
            time_ahead = self.time_ahead
            variables = self.variables
        start_date = self.datetimeToString(model_date + offset)
        end_date = (model_date + time_ahead)
        model_date = self.datetimeToString(model_date)
        end_date = self.datetimeToString(end_date)
        body = {
            "Readers": [{
                "DataSourceCode": self.data_source_id,
                "Settings": {
                    "ModelDate": model_date,
                    "StartDate": start_date,
                    "EndDate": end_date,
                    "VariableCodes": variables,
                    "Extent": {
                        "Xll": self.extent[0],
                        "Yll": self.extent[1],
                        "Xur": self.extent[2],
                        "Yur": self.extent[3],
                        "SpatialReference": {
                            "Epsg": 28992
                        }
                    }
                }
            }],
            "Exporter": {
                "DataFormatCode": "json",
                "Settings": {
                    "Formatting": "Indented"
                }
            }
        }
        return body

    def __parse_json(self, content, output_dict, model_date):
        """Parse json content into dictionary

        Parameters
        ----------
        content : dict
            Json content from download request.
        output_dict : dict
            Output dictionary
        model_date : datetime
            Date of modelrun from download request.
        Returns
        -------
        dict
            Dictionary containing downloaded data.
        """
        for var in content['Data']:
            for grid in var['Grids']:
                if self.shape:
                    data = np.array(grid['Data'])[self.cells].tolist()
                else:
                    data = np.array(grid['Data']).tolist()
                output_dict[self.counter] = [self.stringToDateTime(grid['StartDate']),
                                             model_date,
                                             grid['VariableCode']] + data
                self.counter += 1
        return output_dict

    def download(self):
        """Download data for this data source.
        """
        output_dict = {}
        self.date_range.set_description_str(f'{self.data_source}: Downloading')
        for model_date in self.date_range:
            body = self.__get_body(model_date)
            content = self.make_request(body)
            output_dict = self.__parse_json(content, output_dict, model_date)
        columns = ['StartDate', 'Modeldate', 'Variable'] + self.cells
        output_df = pd.DataFrame.from_dict(output_dict, orient='index', columns=columns)
        self.save_output(output_df)
